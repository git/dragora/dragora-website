#!/bin/bash
# Generate the thumbnails for the Preview section
imgp --recurse --optimize --res 200x200 --quality 85 --adapt --convert
